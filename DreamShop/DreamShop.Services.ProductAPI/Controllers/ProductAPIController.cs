﻿using AutoMapper;
using DreamShop.Services.ProductAPI.Data;
using DreamShop.Services.ProductAPI.Models;
using DreamShop.Services.ProductAPI.Models.Dtos;
using DreamShop.Services.ProductAPI.Models.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DreamShop.Services.ProductAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class ProductAPIController : ControllerBase
    {
        private readonly ProductAPIDbContext _db;
        private readonly IMapper _mapper;
        private readonly ResponseDto _response;

        public ProductAPIController(ProductAPIDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
            _response = new ResponseDto();
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var data = _mapper.Map<List<ProductDto>>(await _db.Products.AsNoTracking().ToListAsync());
                _response.Result = data;
            }
            catch (Exception e)
            {
                _response.IsSuccess = false;
                _response.Message = e.Message;
            }
            return Ok(_response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            if (id == 0) return BadRequest("Invalid input request.");

            try
            {
                _response.Result = _mapper.Map<ProductDto>(await _db.Products.FirstOrDefaultAsync(c => c.Id == id));
            }
            catch (Exception e)
            {
                _response.IsSuccess = false;
                _response.Message = e.Message;
            }

            return Ok(_response);
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Post([FromForm] ProductDto product)
        {
            if (product == null) return BadRequest("Invalid input request.");

            try
            {
                var productToSave = _mapper.Map<Product>(product);
                await _db.Products.AddAsync(productToSave);
                await _db.SaveChangesAsync();

                // save the image
                if (product.Image != null)
                {

                    string fileName = productToSave.Id + Path.GetExtension(product.Image.FileName);
                    string filePath = @"wwwroot\ProductImages\" + fileName;

                    //I have added the if condition to remove the any image with same name if that exist in the folder by any change
                    var directoryLocation = Path.Combine(Directory.GetCurrentDirectory(), filePath);
                    FileInfo file = new FileInfo(directoryLocation);
                    if (file.Exists)
                    {
                        file.Delete();
                    }

                    var filePathDirectory = Path.Combine(Directory.GetCurrentDirectory(), filePath);
                    using (var fileStream = new FileStream(filePathDirectory, FileMode.Create))
                    {
                        product.Image.CopyTo(fileStream);
                    }
                    var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.Value}{HttpContext.Request.PathBase.Value}";
                    productToSave.ImageUrl = baseUrl + "/ProductImages/" + fileName;
                    productToSave.ImageLocalPath = filePath;
                }
                else
                {
                    productToSave.ImageUrl = "https://placehold.co/600x400";
                }

                await _db.SaveChangesAsync();
                _response.Result = _mapper.Map<ProductDto>(productToSave);
            }
            catch (Exception e)
            {
                _response.IsSuccess = false;
                _response.Message = e.Message;
            }

            return Ok(_response);
        }

        [HttpPut]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Put(ProductDto product)
        {
            if (product == null) return BadRequest("Invalid input request.");

            try
            {
                var productToUpdate = _mapper.Map<Product>(product);
                if (product.Image != null)
                {
                    if (!string.IsNullOrEmpty(productToUpdate.ImageLocalPath))
                    {
                        var oldFilePathDirectory = Path.Combine(Directory.GetCurrentDirectory(), product.ImageLocalPath);
                        FileInfo file = new FileInfo(oldFilePathDirectory);
                        if (file.Exists)
                        {
                            file.Delete();
                        }
                    }

                    string fileName = productToUpdate.Id + Path.GetExtension(product.Image.FileName);
                    string filePath = @"wwwroot\ProductImages\" + fileName;
                    var filePathDirectory = Path.Combine(Directory.GetCurrentDirectory(), filePath);
                    using (var fileStream = new FileStream(filePathDirectory, FileMode.Create))
                    {
                        product.Image.CopyTo(fileStream);
                    }
                    var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.Value}{HttpContext.Request.PathBase.Value}";
                    productToUpdate.ImageUrl = baseUrl + "/ProductImages/" + fileName;
                    productToUpdate.ImageLocalPath = filePath;
                }
                _db.Products.Update(productToUpdate);
                await _db.SaveChangesAsync();
                _response.Result = _mapper.Map<ProductDto>(productToUpdate);
            }
            catch (Exception e)
            {
                _response.IsSuccess = false;
                _response.Message = e.Message;
            }

            return Ok(_response);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Delete(long id)
        {
            if (id == 0) return BadRequest("Invalid input request!");

            try
            {
                var dataToDelete = await _db.Products.FirstAsync(c => c.Id == id);
                if (!string.IsNullOrEmpty(dataToDelete.ImageLocalPath))
                {
                    var oldFilePathDirectory = Path.Combine(Directory.GetCurrentDirectory(), dataToDelete.ImageLocalPath);
                    FileInfo file = new FileInfo(oldFilePathDirectory);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                }
                _db.Products.Remove(dataToDelete);
                await _db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _response.IsSuccess = false;
                _response.Message = e.Message;
            }

            return Ok(_response);
        }
    }
}
