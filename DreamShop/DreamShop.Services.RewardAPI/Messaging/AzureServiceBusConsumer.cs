﻿using System.Text;
using Azure.Messaging.ServiceBus;
using DreamShop.Services.RewardAPI.Messages;
using DreamShop.Services.RewardAPI.Messaging;
using DreamShop.Services.RewardAPI.Services;
using Newtonsoft.Json;

namespace DreamShop.Services.RewardAPI.Messaging
{
    public class AzureServiceBusConsumer : IAzureServiceBusConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly RewardService _rewardService;
        private string _serviceBusConn;

        // rewards update after order has been created
        private string _rewardUpdateTopicName;
        private string _rewardUpdateSubscriptionName;
        private ServiceBusProcessor _rewardUpdateProcessor;

        public AzureServiceBusConsumer(IConfiguration configuration, RewardService rewardService)
        {
            _configuration = configuration;
            _rewardService = rewardService;
            _serviceBusConn = _configuration.GetValue<string>("MessageServiceBusConnectionString");
            _rewardUpdateTopicName = _configuration.GetValue<string>("TopicAndQueueNames:OrderCreateTopic");
            _rewardUpdateSubscriptionName = _configuration.GetValue<string>("TopicAndQueueNames:OrderCreatedRewardUpdate");

            var client = new ServiceBusClient(_serviceBusConn);
            _rewardUpdateProcessor = client.CreateProcessor(_rewardUpdateTopicName, _rewardUpdateSubscriptionName);
        }

        public async Task Start()
        {
            // email cart
            _rewardUpdateProcessor.ProcessMessageAsync += OnRewardUpdateReceive;
            _rewardUpdateProcessor.ProcessErrorAsync += OnErrorHandle;
            await _rewardUpdateProcessor.StartProcessingAsync();
        }

        private Task OnErrorHandle(ProcessErrorEventArgs args)
        {
            Console.WriteLine(args.Exception.ToString());
            return Task.CompletedTask;
        }

        private async Task OnRewardUpdateReceive(ProcessMessageEventArgs args)
        {
            // this is where we will get the message from azure service
            var message = args.Message;
            var msgBody = Encoding.UTF8.GetString(message.Body);

            var rewardMessage = JsonConvert.DeserializeObject<RewardMessage>(msgBody);
            try
            {
                await _rewardService.OrderCreatedRewardsUpdate(rewardMessage);
                await args.CompleteMessageAsync(args.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        public async Task Stop()
        {
            await _rewardUpdateProcessor.StopProcessingAsync();
            await _rewardUpdateProcessor.DisposeAsync();
        }
    }
}
