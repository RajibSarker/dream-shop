﻿using DreamShop.Services.ProductAPI.Models;
using DreamShop.Services.RewardAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace DreamShop.Services.RewardAPI.Data
{
    public class RewardAPIDbContext: DbContext
    {
        public RewardAPIDbContext(DbContextOptions<RewardAPIDbContext> options):base(options)
        {
            
        }

        // db sets
        public DbSet<Reward> Rewards { get; set; }

    }
}
