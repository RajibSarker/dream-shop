﻿using AutoMapper;
using DreamShop.Services.RewardAPI.Models.Dtos;

namespace DreamShop.Services.RewardAPI.Models
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<RewardDto, Reward>().ReverseMap();
        }
    }
}
