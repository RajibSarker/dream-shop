﻿namespace DreamShop.Services.RewardAPI.Models.Dtos
{
    public class RewardDto
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public DateTime RewardDate { get; set; }
        public int RewardActivity { get; set; }
        public int OrderId { get; set; }
    }
}
