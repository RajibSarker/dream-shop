﻿
using DreamShop.Services.RewardAPI.Messages;
using DreamShop.Services.RewardAPI.Models.Dtos;

namespace DreamShop.Services.RewardAPI.Services
{
    public interface IRewardService
    {
        Task OrderCreatedRewardsUpdate(RewardMessage reward);
    }
}
