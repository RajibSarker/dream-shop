﻿using System.Text;
using DreamShop.Services.RewardAPI.Data;
using DreamShop.Services.RewardAPI.Messages;
using DreamShop.Services.RewardAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DreamShop.Services.RewardAPI.Services
{
    public class RewardService : IRewardService
    {
        private readonly DbContextOptions<RewardAPIDbContext> _options;

        public RewardService(DbContextOptions<RewardAPIDbContext> options)
        {
            _options = options;
        }

        public async Task OrderCreatedRewardsUpdate(RewardMessage reward)
        {
            try
            {
                Reward rewardToSave = new Reward()
                {
                    OrderId = reward.OrderId,
                    RewardDate = DateTime.Now,
                    RewardActivity = reward.RewardActivity,
                    UserId = reward.UserId,
                };

                await using var db = new RewardAPIDbContext(_options);
                await db.Rewards.AddAsync(rewardToSave);
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
