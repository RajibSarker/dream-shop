using AutoMapper;
using DreamShop.Services.RewardAPI.Data;
using DreamShop.Services.RewardAPI.Extensions;
using DreamShop.Services.RewardAPI.Messaging;
using DreamShop.Services.RewardAPI.Models;
using DreamShop.Services.RewardAPI.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();



// register db context
builder.Services.AddDbContext<RewardAPIDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));


var dbContextOptionsBuilder = new DbContextOptionsBuilder<RewardAPIDbContext>();
dbContextOptionsBuilder.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
builder.Services.AddSingleton(new RewardService(dbContextOptionsBuilder.Options));

// service registration
builder.Services.AddSingleton<IAzureServiceBusConsumer, AzureServiceBusConsumer>();

// auto mapper configuration
var mappingConfig = new MapperConfiguration(mc =>
{
    mc.AddProfile(new MappingProfile());
});
IMapper mapper = mappingConfig.CreateMapper();
builder.Services.AddSingleton(mapper);


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();
app.UseAzureServiceBusConsuser();

app.Run();
