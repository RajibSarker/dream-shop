﻿using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authentication;

namespace DreamShop.Services.ShoppingCartAPI.Utilities
{
    public class APIAuthenticationHttpClientHandler: DelegatingHandler      //TODO: further study on DelegatingHandler
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public APIAuthenticationHttpClientHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var token = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            return await base.SendAsync(request, cancellationToken);
        }
    }
}
