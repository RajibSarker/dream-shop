﻿using DreamShop.Services.EmailAPI.Messages;
using DreamShop.Services.EmailAPI.Models.Dto;

namespace DreamShop.Services.EmailAPI.Services
{
    public interface IEmailService
    {
        Task EmailCartAndLog(CartHeaderDto cartHeaderDto);
        Task NewUserRegistrationAndLog(string userRegistrationMsg);
        Task OrderPlacedLogged(RewardMessage rewardMessage);
    }
}
