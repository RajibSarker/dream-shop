﻿using System.Text;
using DreamShop.Services.EmailAPI.Data;
using DreamShop.Services.EmailAPI.Messages;
using DreamShop.Services.EmailAPI.Models;
using DreamShop.Services.EmailAPI.Models.Dto;
using Microsoft.EntityFrameworkCore;

namespace DreamShop.Services.EmailAPI.Services
{
    public class EmailService : IEmailService
    {
        private readonly DbContextOptions<EmailAPIDbContext> _options;

        public EmailService(DbContextOptions<EmailAPIDbContext> options)
        {
            _options = options;
        }

        public async Task EmailCartAndLog(CartHeaderDto cartHeaderDto)
        {
            StringBuilder message = new StringBuilder();
            message.AppendLine("<br/>Cart Email Requested ");
            message.AppendLine("<br/>Cart Total: " + cartHeaderDto.CartTotal);
            message.AppendLine("<br/>");
            message.AppendLine("<ul>");
            foreach (var details in cartHeaderDto.CartDetails)
            {
                message.AppendLine("<li>");
                message.AppendLine(details.Product.Name + " X " + details.Product.Price);
                message.AppendLine("</li>");
            }

            message.AppendLine("</ul>");
            await LogAndEmail(message.ToString(), cartHeaderDto.Email);
        }

        public async Task NewUserRegistrationAndLog(string userRegistrationMsg)
        {
            string message = $"New user has been registered with <br> Email: " + userRegistrationMsg;
            await LogAndEmail(message, "rajibsarker320@gmail.com");  
        }

        public async Task OrderPlacedLogged(RewardMessage rewardMessage)
        {
            string message = $"New order has been approved with <br> OrderId: " + rewardMessage.OrderId;
            await LogAndEmail(message, "rajibsarker320@gmail.com");
        }

        private async Task<bool> LogAndEmail(string message, string email)
        {
            try
            {
                EmailLogger logger = new EmailLogger()
                {
                    Email = email,
                    Message = message,
                    MessageSent = DateTime.Now
                };

                await using var db = new EmailAPIDbContext(_options);
                await db.EmailLoggers.AddAsync(logger);
                return await db.SaveChangesAsync() > 0;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
