﻿namespace DreamShop.Services.EmailAPI.Models
{
    public class EmailLogger
    {
        public long Id { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }
        public DateTime? MessageSent { get; set; }
    }
}
