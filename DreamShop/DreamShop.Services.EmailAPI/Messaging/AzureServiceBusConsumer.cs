﻿using System.Text;
using Azure.Messaging.ServiceBus;
using DreamShop.Services.EmailAPI.Messages;
using DreamShop.Services.EmailAPI.Models.Dto;
using DreamShop.Services.EmailAPI.Services;
using Newtonsoft.Json;

namespace DreamShop.Services.EmailAPI.Messaging
{
    public class AzureServiceBusConsumer : IAzureServiceBusConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly EmailService _emailService;
        private string _serviceBusConn;

        // email cart
        private string _emailCartQueueName;
        private ServiceBusProcessor _emailCartProcessor;

        // new user registration
        private string _newUserRegistrationQueueName;
        private ServiceBusProcessor _newUserRegistrationProcessor;

        // order confirmed logged
        private string _orderConfirmedTopicName;
        private string _orderConfirmedSubscriptionName;
        private ServiceBusProcessor _orderConfirmedSubscriptionProcessor;

        public AzureServiceBusConsumer(IConfiguration configuration, EmailService emailService)
        {
            _configuration = configuration;
            _emailService = emailService;
            _serviceBusConn = _configuration.GetValue<string>("MessageServiceBusConnectionString");
            _emailCartQueueName = _configuration.GetValue<string>("TopicAndQueueNames:EmailShoppingCartQueue");
            _newUserRegistrationQueueName = _configuration.GetValue<string>("TopicAndQueueNames:NewUserRegistration");

            var client = new ServiceBusClient(_serviceBusConn);
            _emailCartProcessor = client.CreateProcessor(_emailCartQueueName);
            _newUserRegistrationProcessor = client.CreateProcessor(_newUserRegistrationQueueName);

            _orderConfirmedTopicName = _configuration.GetValue<string>("TopicAndQueueNames:OrderCreateTopic");
            _orderConfirmedSubscriptionName = _configuration.GetValue<string>("TopicAndQueueNames:OrderCreatedEmailSendSubscription");
            _orderConfirmedSubscriptionProcessor =
                client.CreateProcessor(_orderConfirmedTopicName, _orderConfirmedSubscriptionName);
        }

        public async Task Start()
        {
            // email cart
            _emailCartProcessor.ProcessMessageAsync += OnEmailCartReceived;
            _emailCartProcessor.ProcessErrorAsync += OnErrorHandle;
            await _emailCartProcessor.StartProcessingAsync();

            // new user registration
            _newUserRegistrationProcessor.ProcessMessageAsync += OnNewUserRegistrationReceived;
            _newUserRegistrationProcessor.ProcessErrorAsync += OnErrorHandle;
            await _newUserRegistrationProcessor.StartProcessingAsync();

            // order confirmed logged
            _orderConfirmedSubscriptionProcessor.ProcessMessageAsync += OnNewOrderConfirmedReceived;
            _orderConfirmedSubscriptionProcessor.ProcessErrorAsync += OnErrorHandle;
            await _orderConfirmedSubscriptionProcessor.StartProcessingAsync();
        }

        private async Task OnNewUserRegistrationReceived(ProcessMessageEventArgs args)
        {
            // this is where we will get the message from azure service
            var message = args.Message;
            var msgBody = Encoding.UTF8.GetString(message.Body);

            var userRegistrationMsg = JsonConvert.DeserializeObject<string>(msgBody);
            try
            {
                //TODO: try to log email
                await _emailService.NewUserRegistrationAndLog(userRegistrationMsg);
                await args.CompleteMessageAsync(args.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private Task OnErrorHandle(ProcessErrorEventArgs args)
        {
            Console.WriteLine(args.Exception.ToString());
            return Task.CompletedTask;
        }

        private async Task OnEmailCartReceived(ProcessMessageEventArgs args)
        {
            // this is where we will get the message from azure service
            var message = args.Message;
            var msgBody = Encoding.UTF8.GetString(message.Body);

            var cart = JsonConvert.DeserializeObject<CartHeaderDto>(msgBody);
            try
            {
                //TODO: try to log email
                await _emailService.EmailCartAndLog(cart);
                await args.CompleteMessageAsync(args.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        private async Task OnNewOrderConfirmedReceived(ProcessMessageEventArgs args)
        {
            // this is where we will get the message from azure service
            var message = args.Message;
            var msgBody = Encoding.UTF8.GetString(message.Body);

            var rewardMessage = JsonConvert.DeserializeObject<RewardMessage>(msgBody);
            try
            {
                await _emailService.OrderPlacedLogged(rewardMessage);
                await args.CompleteMessageAsync(args.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        public async Task Stop()
        {
            await _emailCartProcessor.StopProcessingAsync();
            await _emailCartProcessor.DisposeAsync();

            await _newUserRegistrationProcessor.StopProcessingAsync();
            await _newUserRegistrationProcessor.DisposeAsync();
        }
    }
}
