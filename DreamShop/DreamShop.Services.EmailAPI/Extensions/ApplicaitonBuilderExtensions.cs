﻿using DreamShop.Services.EmailAPI.Messaging;

namespace DreamShop.Services.EmailAPI.Extensions
{
    public static class ApplicaitonBuilderExtensions
    {
        private static IAzureServiceBusConsumer _azureServiceBusConsumer { get; set; }
        public static IApplicationBuilder UseAzureServiceBusConsuser(this IApplicationBuilder applicaitonBuilder)
        {
            _azureServiceBusConsumer = applicaitonBuilder.ApplicationServices.GetService<IAzureServiceBusConsumer>();
            var hostLifeTime = applicaitonBuilder.ApplicationServices.GetService<IHostApplicationLifetime>();

            hostLifeTime.ApplicationStarted.Register(OnStart);
            hostLifeTime.ApplicationStopped.Register(OnStop);

            return applicaitonBuilder;  
        }

        private static void OnStop()
        {
            _azureServiceBusConsumer.Stop();
        }

        private static void OnStart()
        {
            _azureServiceBusConsumer.Start();
        }
    }
}
