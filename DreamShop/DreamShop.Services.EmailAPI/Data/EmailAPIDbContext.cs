﻿using DreamShop.Services.EmailAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace DreamShop.Services.EmailAPI.Data
{
    public class EmailAPIDbContext: DbContext
    {
        public EmailAPIDbContext(DbContextOptions<EmailAPIDbContext> options):base(options)
        {
            
        }

        // db sets
        public DbSet<EmailLogger> EmailLoggers { get; set; }

    }
}
