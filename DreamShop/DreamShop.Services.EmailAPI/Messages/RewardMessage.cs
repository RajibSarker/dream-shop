﻿namespace DreamShop.Services.EmailAPI.Messages
{
    public class RewardMessage
    {
        public string UserId { get; set; }
        public int RewardActivity { get; set; }
        public long OrderId { get; set; }
    }
}
