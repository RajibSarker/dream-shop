﻿using AutoMapper;
using Azure;
using DreamShop.MessageBus;
using DreamShop.Services.OrderAPI.Data;
using DreamShop.Services.OrderAPI.Models;
using DreamShop.Services.OrderAPI.Models.Dto;
using DreamShop.Services.OrderAPI.Models.DTOs;
using DreamShop.Services.OrderAPI.Services.Interface;
using DreamShop.Services.OrderAPI.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DreamShop.Services.OrderAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderAPIController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly OrderAPIDbContext _db;
        private readonly IMessageBus _messageBus;
        private readonly IConfiguration _configuration;
        private readonly IProductService _productService;
        protected ResponseDto _responseDto;

        public OrderAPIController(IMapper mapper, OrderAPIDbContext db, IMessageBus messageBus, IConfiguration configuration, IProductService productService)
        {
            _mapper = mapper;
            _db = db;
            _messageBus = messageBus;
            _configuration = configuration;
            _productService = productService;
            _responseDto = new ResponseDto();
        }

        [Authorize]
        [HttpGet("getOrders")]
        public async Task<ResponseDto> GetOrders(string userId = "")
        {
            try
            {
                IEnumerable<OrderHeader> orders;
                if (User.IsInRole(StaticDetails.RoleAdmin))
                {
                    orders = await _db.OrderHeaders.Include(c => c.OrderDetails).OrderByDescending(c => c.Id).AsNoTracking().ToListAsync();
                }
                else
                {
                    orders = await _db.OrderHeaders.Include(c => c.OrderDetails).Where(c=> c.UserId == userId).OrderByDescending(c=> c.Id).AsNoTracking().ToListAsync();
                }

                _responseDto.Result = _mapper.Map<IEnumerable<OrderHeaderDto>>(orders);
            }
            catch (Exception e)
            {
                _responseDto.IsSuccess = false;
                _responseDto.Message = e.Message;
            }

            return _responseDto;
        }

        [Authorize]
        [HttpGet("getOrder/{orderId:long}")]
        public async Task<ResponseDto> GetOrderById(long orderId)
        {
            var orToReturn = await _db.OrderHeaders.Include(c => c.OrderDetails).FirstOrDefaultAsync(c => c.Id == orderId);
            _responseDto.Result = _mapper.Map<OrderHeaderDto>(orToReturn);
            return _responseDto;
        }

        [Authorize]
        [HttpPost("CreateOrder")]
        public async Task<ResponseDto> CreateOrder([FromBody] CartHeaderDto cart)
        {
            try
            {
                OrderHeaderDto orderHeaderDto = _mapper.Map<OrderHeaderDto>(cart);
                orderHeaderDto.DateTime = DateTime.Now;
                orderHeaderDto.Status = StaticDetails.Status_Pending;
                orderHeaderDto.OrderDetails = _mapper.Map<ICollection<OrderDetailsDto>>(cart.CartDetails);

                var orderToCreate = _mapper.Map<OrderHeader>(orderHeaderDto);
                await _db.OrderHeaders.AddAsync(orderToCreate);
                await _db.SaveChangesAsync();

                //TODO: stripe payment gateway integration

                // rewards update
                var topicName = _configuration.GetValue<string>("TopicAndQueueNames:OrderCreatedTopic");
                await _messageBus.PublishMessage(new RewardDto()
                {
                    OrderId = orderToCreate.Id,
                    RewardActivity = Convert.ToInt32(orderToCreate.OrderTotal),
                    UserId = orderToCreate.UserId
                }, topicName);

                // update the order status after payment
                orderToCreate.Status = StaticDetails.Status_Approved;
                await _db.SaveChangesAsync();

                _responseDto.Result = _mapper.Map<OrderHeaderDto>(orderToCreate);
            }
            catch (Exception e)
            {
                _responseDto.IsSuccess = false;
                _responseDto.Message = e.Message;
            }

            return _responseDto;
        }

        [Authorize]
        [HttpPost("updatestatus/{orderId:long}")]
        public async Task<ResponseDto> UpdateStatus(long orderId, [FromBody] string status)
        {
            try
            {
                var existingOrder = await _db.OrderHeaders.FirstOrDefaultAsync(c => c.Id == orderId);
                if (existingOrder is null)
                {
                    _responseDto.IsSuccess = false;
                    _responseDto.Message = "Order details could not be found at this time! Please try again later.";
                    return _responseDto;
                }

                if (status == StaticDetails.Status_Cancelled)
                {
                    //TODO: apply refund policy
                }
                existingOrder.Status = status;
                await _db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _responseDto.IsSuccess = false;
                _responseDto.Message = e.Message;
            }

            return _responseDto;
        }
    }
}
