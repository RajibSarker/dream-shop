﻿namespace DreamShop.Services.OrderAPI.Models.Dto
{
    public class RewardDto
    {
        public string UserId { get; set; }
        public int RewardActivity { get; set; }
        public long OrderId { get; set; }
    }
}
