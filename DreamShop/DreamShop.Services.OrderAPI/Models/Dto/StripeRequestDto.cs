﻿using DreamShop.Services.OrderAPI.Models.Dto;

namespace DreamShop.Services.OrderAPI.Models.DTOs
{
    public class StripeRequestDto
    {
        public string StripeSessionUrl { get; set; }
        public string StripeSessionId { get; set; }
        public string ApprovedUrl { get; set; }
        public String CancelUrl { get; set; }
        public CartHeaderDto CartHeader { get; set; }
    }
}
