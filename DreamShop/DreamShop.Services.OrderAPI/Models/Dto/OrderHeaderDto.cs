﻿namespace DreamShop.Services.OrderAPI.Models.Dto
{
    public class OrderHeaderDto
    {
        public OrderHeaderDto()
        {
            OrderDetails = new List<OrderDetailsDto>();
        }
        public long Id { get; set; }
        public string? UserId { get; set; }
        public string? CouponCode { get; set; }
        public double Discount { get; set; }
        public double OrderTotal { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
        public DateTime DateTime { get; set; }
        public string? Status { get; set; }
        public string? PaymentIntentId { get; set; }
        public string? StripeSessionId { get; set; }
        public ICollection<OrderDetailsDto> OrderDetails { get; set; }
    }
}
