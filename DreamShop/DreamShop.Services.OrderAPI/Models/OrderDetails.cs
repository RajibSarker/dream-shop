﻿using System.ComponentModel.DataAnnotations.Schema;
using DreamShop.Services.OrderAPI.Models.Dtos;

namespace DreamShop.Services.OrderAPI.Models
{
    public class OrderDetails
    {
        public long Id { get; set; }
        public long OrderHeaderId { get; set; }
        public OrderHeader? OrderHeader { get; set; }
        public long ProductId { get; set; }
        [NotMapped]
        public ProductDto? Product { get; set; }
        public int Count { get; set; }
        public string ProductName { get; set; }
        public double Price { get; set; }
    }
}
