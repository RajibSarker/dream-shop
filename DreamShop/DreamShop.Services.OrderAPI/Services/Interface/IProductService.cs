﻿
using DreamShop.Services.OrderAPI.Models.Dtos;

namespace DreamShop.Services.OrderAPI.Services.Interface
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDto>> GetProductsAsync();
    }
}
