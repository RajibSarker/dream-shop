﻿using System.Text.Json.Serialization;
using DreamShop.Services.OrderAPI.Models.Dtos;
using DreamShop.Services.OrderAPI.Models.DTOs;
using DreamShop.Services.OrderAPI.Services.Interface;
using Newtonsoft.Json;

namespace DreamShop.Services.OrderAPI.Services
{
    public class ProductService: IProductService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public ProductService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task<IEnumerable<ProductDto>> GetProductsAsync()
        {
            var client = _httpClientFactory.CreateClient("product");
            var response = await client.GetAsync("api/ProductAPI");
            var apiContent = await response.Content.ReadAsStringAsync();    
            var data = JsonConvert.DeserializeObject<ResponseDto>(apiContent);
            if (data.IsSuccess)
            {
                var products = JsonConvert.DeserializeObject<IEnumerable<ProductDto>>(data.Result.ToString());
                return products;
            }

            return new List<ProductDto>();
        }
    }
}
