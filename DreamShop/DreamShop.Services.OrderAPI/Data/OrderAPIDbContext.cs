﻿using DreamShop.Services.OrderAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace DreamShop.Services.OrderAPI.Data
{
    public class OrderAPIDbContext: DbContext
    {
        public OrderAPIDbContext(DbContextOptions<OrderAPIDbContext> options):base(options)
        {
            
        }

        // db sets
        public DbSet<OrderHeader> OrderHeaders { get; set; }
        public DbSet<OrderDetails> OrderDetails { get; set; }

    }
}
