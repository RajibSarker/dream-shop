﻿using System.ComponentModel.DataAnnotations;

namespace DreamShop.Web.Models.DTOs
{
    public class CartHeaderDto
    {
        public CartHeaderDto()
        {
            CartDetails = new List<CartDetailsDto>();
        }

        public long Id { get; set; }
        public string? UserId { get; set; }
        public string? CouponCode { get; set; }
        public double Discount { get; set; }
        public double CartTotal { get; set; }
        [Required]
        public string? FirstName { get; set; }
        [Required]
        public string? LastName { get; set; }
        [Required]
        public string? Phone { get; set; }
        [Required]
        public string? Email { get; set; }
        public ICollection<CartDetailsDto> CartDetails { get; set; }
    }
}