﻿namespace DreamShop.Web.Models.DTOs
{
    public class OrderDetailsDto
    {
        public long Id { get; set; }
        public long OrderHeaderId { get; set; }
        public OrderHeaderDto? OrderHeader { get; set; }
        public long ProductId { get; set; }
        public ProductDto? Product { get; set; }
        public int Count { get; set; }
        public string ProductName { get; set; }
        public double Price { get; set; }
    }
}
