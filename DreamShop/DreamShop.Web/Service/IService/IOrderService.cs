﻿using DreamShop.Web.Models;
using DreamShop.Web.Models.DTOs;

namespace DreamShop.Web.Service.IService
{
    public interface IOrderService
    {
        Task<ResponseDto> CreateOrderAsync(CartHeaderDto cartHeaderDto);
        Task<ResponseDto> GetAllOrdersAsync(string? userId);
        Task<ResponseDto> GetOrderByIdAsync(long orderId);
        Task<ResponseDto> UpdateOrderStatusAsync(long orderId, string status);
    }
}
