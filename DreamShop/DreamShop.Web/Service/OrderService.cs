﻿using DreamShop.Web.Models;
using DreamShop.Web.Models.DTOs;
using DreamShop.Web.Service.IService;
using DreamShop.Web.Utilities;

namespace DreamShop.Web.Service
{
    public class OrderService: IOrderService
    {
        private readonly IBaseService _baseService;
        public OrderService(IBaseService baseService)
        {
            _baseService = baseService;
        }
        public async Task<ResponseDto> CreateOrderAsync(CartHeaderDto cartHeaderDto)
        {
            return await _baseService.SendAsync(new RequestDto()
            {
                Data = cartHeaderDto,
                APIType = APIType.POST,
                Url = Utility.OrderAPIBaseUrl + "api/orderAPI/CreateOrder"
            }, true);
        }

        public async Task<ResponseDto> GetAllOrdersAsync(string? userId)
        {
            return await _baseService.SendAsync(new RequestDto()
            {
                Data = userId,
                APIType = APIType.GET,
                Url = Utility.OrderAPIBaseUrl + "api/orderAPI/getOrders?userId="+userId
            }, true);
        }

        public async Task<ResponseDto> GetOrderByIdAsync(long orderId)
        {
            return await _baseService.SendAsync(new RequestDto()
            {
                APIType = APIType.GET,
                Url = Utility.OrderAPIBaseUrl + "api/orderAPI/getOrder/" + orderId
            }, true);
        }

        public async Task<ResponseDto> UpdateOrderStatusAsync(long orderId, string status)
        {
            return await _baseService.SendAsync(new RequestDto()
            {
                Data = status,
                APIType = APIType.POST,
                Url = Utility.OrderAPIBaseUrl + "api/orderAPI/updatestatus/" + orderId
            }, true);
        }
    }
}
