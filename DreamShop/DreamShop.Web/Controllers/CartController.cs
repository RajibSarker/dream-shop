﻿using DreamShop.Web.Models.DTOs;
using DreamShop.Web.Service.IService;
using DreamShop.Web.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;

namespace DreamShop.Web.Controllers
{
    public class CartController : Controller
    {
        private readonly ICartService _cartService;
        private readonly IOrderService _orderService;

        public CartController(ICartService cartService, IOrderService orderService)
        {
            _cartService = cartService;
            _orderService = orderService;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            return View(await GetCartForLoggedInUser());
        }

        [Authorize]
        public async Task<IActionResult> Checkout()
        {
            return View(await GetCartForLoggedInUser());
        }

        [HttpPost]
        [ActionName("Checkout")]
        public async Task<IActionResult> Checkout(CartHeaderDto cartHeaderDto)
        {
            var cart = await GetCartForLoggedInUser();
            cart.FirstName = cartHeaderDto.FirstName;
            cart.LastName = cartHeaderDto.LastName;
            cart.Email = cartHeaderDto.Email;
            cart.Phone = cartHeaderDto.Phone;

            var response = await _orderService.CreateOrderAsync(cart);
            if (response != null && response.IsSuccess)
            {
                var order = Utility.MapToResponse<OrderHeaderDto>(response.Result);
                TempData["success"] = "Order has been created.";
                return RedirectToAction(nameof(Confirmation), new{OrderId = order.Id});
            }
            else
            {
                TempData["error"] = "Order could not be created! Please try again later.";
            }

            return View(cartHeaderDto);
        }

        public async Task<IActionResult> Confirmation(long orderId)
        {
            return View(orderId);
        }

        [HttpPost]
        public async Task<IActionResult> ApplyCoupon(CartHeaderDto cart)
        {
            var response = await _cartService.AddCoupon(cart);
            if (response != null && response.IsSuccess)
            {
                TempData["success"] = "Coupon added successfully.";
                return RedirectToAction(nameof(Index));
            }
            else
            {
                TempData["error"] = "Coupon added failed!";
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> EmailCart(CartHeaderDto cart)
        {
            var cartData = await GetCartForLoggedInUser();
            cartData.Email = User.Claims.Where(c => c.Type == JwtRegisteredClaimNames.Email)?.FirstOrDefault()?.Value;
            
            // email logging using azure service bus
            var response = await _cartService.CartEmailRequest(cartData);

            if (response != null && response.IsSuccess)
            {
                TempData["success"] = "Email will be processed and send shortly.";
                return RedirectToAction(nameof(Index));
            }
            else
            {
                TempData["error"] = "Email sent failed!";
            }

            return View(cart);
        }


        public async Task<IActionResult> Remove(long detailsId)
        {
            var response = await _cartService.RemoveCart(detailsId);
            if (response != null && response.IsSuccess)
            {
                TempData["success"] = "Item deleted from cart successfully.";
                return RedirectToAction(nameof(Index));
            }
            else
            {
                TempData["error"] = "Items cart delete operation failed!";
            }

            return View();
        }

        public async Task<CartHeaderDto> GetCartForLoggedInUser()
        {
            var userId = User.Claims.Where(c => c.Type == JwtRegisteredClaimNames.Sub)?.FirstOrDefault()?.Value;
            var response = await _cartService.GetCartsByUserIdAsync(userId);
            if (response != null)
            {
                var data = Utility.MapToResponse<CartHeaderDto>(response.Result);
                return data;
            }

            return new();
        }
    }
}
