﻿using DreamShop.MessageBus;
using DreamShop.Services.AuthAPI.Models.DTOs;
using DreamShop.Services.AuthAPI.Services.IServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.Data;
using Microsoft.AspNetCore.Mvc;

namespace DreamShop.Services.AuthAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly IMessageBus _messageBus;
        private readonly IConfiguration _configuration;
        ResponseDto _responseDto;

        public AuthController(IAuthService authService, IMessageBus messageBus, IConfiguration configuration)
        {
            _authService = authService;
            _messageBus = messageBus;
            _configuration = configuration;
            _responseDto = new();
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] UserRegistrationRequestDto requestDto)
        {
            if (requestDto == null) return BadRequest("Invalid input request.");

            var isError = await _authService.UserRegistration(requestDto);
            if (string.IsNullOrEmpty(isError))
            {
                _responseDto.IsSuccess = false;
                _responseDto.Message = isError;
                return BadRequest(_responseDto);
            }

            // email logger after user registration
            await _messageBus.PublishMessage(requestDto.Email,
                _configuration.GetValue<string>("TopicAndQueueNames:NewUserRegistration"));

            return Ok(_responseDto);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] UserLoginRequestDto requestDto)
        {
            var isLoggedIn = await _authService.UserLogin(requestDto);
            if (isLoggedIn.User is null)
            {
                _responseDto.IsSuccess = false;
                _responseDto.Message = "User name or password does not matched!";
                return Unauthorized(_responseDto);
            }

            _responseDto.Result = isLoggedIn;
            return Ok(_responseDto);
        }

        [HttpPost("assignUserRole")]
        public async Task<IActionResult> AssignUserRole([FromBody] UserRegistrationRequestDto requestDto)
        {
            var result = await _authService.AssignUserRole(requestDto.Email, requestDto.Role.ToUpper());
            if (!result)
            {
                _responseDto.IsSuccess = false;
                _responseDto.Message = "Error occured to assign role";
            }
            return Ok(_responseDto);
        }
    }
}
